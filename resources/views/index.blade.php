<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Qazaq Republic</title>
	<link rel="stylesheet" type="text/css" href="/styles/styles.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<script type="text/javascript" href="/scripts/script.js"></script>
</head>
<body >
<header class="header">
    <div class="header-first">
        <a href="#" class="logo"><img src="/images/2.png" alt="QR" ></a>
        <form action="" class="search-form">
            <input type="search" name="" placeholder="search..." id="search-box">
            <label for="search-box" class="fas fa-search"></label>
        </form>
        <div class="icons">
            <div id="search-btn" class="fas fa-search"></div>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-shopping-cart"></a>
            <div id="login-btn" class="fas fa-user"></div>
        </div>
        <nav class="navbar">
            <a href="#home"><b>Home</b></a>
            <a href="#clothes"><b>Store</b></a>
            <!--<a href="#looks"><b>About us</b></a>-->
            <a href="#about"><b>Looks</b> </a>
        </nav>
    </div>
</header>
<nav class="bottom-nav">
    <a href="#home" class="fas fa-home"></a>
    <a href="#clothes" class="fas fa-tshirt"></a>
    <a href="#looks" class="fas fa-shopping-bag"></a>
    <a href="#about" class="fas fa-address-book"></a>
</nav>
<div class="login-form">
	    <div id="close-btn" class="fas fa-times"></div>
	<form action="">
		<h3>Sign in</h3>
		<span>Username</span>
		<input type="phone number" class="placeholder" name="" placeholder="enter phone number" id="">
		<span>Password</span>
		<input type="password" class="placeholder" name="" placeholder="enter your password" id="">   0
		<div class="checkbox">
			<input type="checkbox" id="remember" >
			<label for="remember">Remember me?</label>
		</div>
	<input type="submit" value="sign in" class="btn">
	<p>Forgot your password?<a href="#">click here</a></p>
	<p>You don't have an account?<a href="#">register</a></p>
	</form>
</div>
<section class="home" id="home">
	<div class="content">
        <h2><b> {{ $banner->title }} </b></h2>
        <h1><b> {{ $banner->subtitle }} </b></h1>
        <p><b> {{ $banner->description }} </b></p>
</section>
<section class="clothes" id="clothes">
    <h3 class="sub-heading"> QR online store concept</h3>
    <h1 class="headin-tes"> <span> "Store" </span> </h1>
    <div class="box-container">
    @foreach($products as $product)
        <div class="box">
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
            <img src="/storage/{{$product->image}}" alt="shirts">
            <h3>{{$product->name}}</h3>
            <h4>{{$product->category}}</h4>
            <br>
            <p>{{$product->price}}</p>
            <!-- <a href="#" class="btn">Add card</a> -->
        </div>
    @endforeach
    </div>
</section>
<section class="about" id="about">
	<section class="about" id="about">
	<h1 class="heading-tesi"> "Looks" </h1>
	<div class="swiper about-slider">
        <div class="swiper-wrapper">
            <div class="swiper slide-box">
                <div class="image">
                    <img src="/images/first.jpg" class="image-one" alt="2person">
                </div>
                <div class="info">
                    <h3 class="info-bur">Delivery Almaty/Nūr-sūltan</h3>
                    <p class="info-bre"> From 15 000 tenge delivery is free.</br> We deliver every day without days off.</br>The order will be delivered on the same day</p>
                    <div class="button-read">
                        <a href="" class="btn-des">Read more</a>
                    </div>    
                </div>
            </div>
            <div class="swiper slide-box">
                <div class="info">
                    <h3 class="info-burl">Delivery to other cities</h3>
                    <p class="info-brex">Delivery times 3-5 working days (large cities), 6-14 (remote regions)!</p>
                    <div class="button-read">
                        <a href="" class="btn-chet">Read more</a>
                    </div>    
                </div>
                <div class="image">
                    <img src="/images/second.jpg" class="image-second" alt="2person">
                </div>
            </div>
            <div class="swiper slide-box">
                <div class="image">
                    <img src="/images/third.jpg" class="image-third" alt="2person">
                </div>
                <div class="info">
                    <h3 class="info-burg">Payment methods</h3>
                    <p class="info-bank"><ul>
                    	<li class="info-bank">- Kaspi red - </li>
                    	<li class="info-bank">- PayPal - </li>
                    	<li class="info-bank">- Mastercard - </li>
                    	<li class="info-bank">- Visa - </li>
                    </ul></p>
                    <div class="button-read">
                        <a href="" class="btn-des">Read more</a>
                    </div>    
                </div>
            </div>
            <div class="swiper slide-box">
                <div class="info">
                    <h3 class="info-burk">Order</h3>
                    <p class="info-bret">Manage to place an order before December 24</br> to have time to receive them before the New Year.</p>
                    <div class="button-read">
                        <a href="" class="btn-dek">Read more</a>
                    </div>    
                </div>
                <div class="image">
                    <img src="/images/fourth.jpg" class="image-fourth" alt="2person">
                </div>
            </div>
            <div class="swiper slide-box">
                <div class="image">
                    <img src="/images/fifth.jpg" class="image-five" alt="2person">
                </div>
                <div class="info">
                    <h3 class="info-bura">LifeHack</h3>
                    <p class="info-banke">To find the correct size, take measurements </br>of the waist circumference and divide by two.</p>
                    <div class="button-read"></div>
                        <a href="" class="btn-sel">Read more</a>
                    </div>
                </div>
            </div>
    </div>
<div class="footer">
    <div class="footer-container">
        <div class="loc">
            <h3 class="sub-heading"> our locations </h3>
            @foreach($locations as $location)
                <a href="{{$location->link}}"> <i class="fas fa-map-marker-alt"></i> {{$location->name}} </a>
            @endforeach
        </div>
             <div class="loc">
            <h3 class="sub-heading"> contact info </h3>
            @foreach($phones as $phone)
                <a href="tel:{{$phone->phone}}"> <i class="fas fa-phone"></i>{{$phone->phone}}</a>
            @endforeach
           
        </div>
    </div>
    <div class="share">
        <a href="https://www.instagram.com/qazaq.republic" class="fab fa-instagram "target="_blank"></a>
        <a href="https://taplink.cc/qazaq.republic" class="fab fa-whatsapp" target="_blank"></a>
        <a href="https://t.me/s/WeAreQR" class="fab fa-telegram" target="_blank"></a>
        <a href="https://www.tiktok.com/@qazaq.republic?" class="fab fa-tiktok" target="_blank"></a>
    </div>
    <div class="made"> Made with love by Medina and Erkebulan </div>
</section>
</body>
</html>